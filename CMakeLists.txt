project(MineLight)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)

find_package(Threads REQUIRED)
find_package(fmt REQUIRED)


add_executable(test-connect
	test-connect/main.cxx
	test-connect/connection.cxx
)

target_include_directories(test-connect PRIVATE
	libnz
	libnz/common
)

target_link_libraries(test-connect
	fmt
	Threads::Threads
)


add_executable(test-client
	test-client/buffer-pool.cxx
	test-client/main.cxx
	test-client/udp-connection.cxx
	import/sha256.c
	import/srp.c
)

target_include_directories(test-client PRIVATE
	libnz
	libnz/common
)

target_link_libraries(test-client
	fmt
	Threads::Threads
	gmp
)


add_executable(test-break)
if(WIN32)
	target_sources(test-break PRIVATE test-break/main-win32.cxx)
else()
	target_sources(test-break PRIVATE test-break/main.cxx)
endif()

target_include_directories(test-break PRIVATE
	libnz
	libnz/common
)

target_link_libraries(test-break
	fmt
	Threads::Threads
)
