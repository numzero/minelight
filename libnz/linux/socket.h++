#pragma once
// C++
#include <initializer_list>
// Local
#include "file.h++"
#include "socket.hxx"

namespace nz {
namespace unistd {

class Socket: public File
{
public:
	using File::File;
};

inline buffer_ref recv(Socket const &socket, buffer_ref buf, int flags = 0) {
	return recv(socket.get(), buf, flags);
}

inline std::pair<buffer_ref, buffer_ref> recvfrom(Socket const &socket, buffer_ref buf, buffer_ref addr_buf, int flags = 0) {
	return recvfrom(socket.get(), buf, addr_buf, flags);
}

template <typename Addr>
inline buffer_ref recvfrom(Socket const &socket, buffer_ref buf, Addr &addr, int flags = 0) {
	return recvfrom(socket.get(), buf, addr, flags);
}

inline size_t send(Socket const &socket, const_buffer_ref buf, int flags = 0) {
	return send(socket.get(), buf, flags);
}

inline size_t sendto(Socket const &socket, const_buffer_ref buf, const_buffer_ref addr, int flags = 0) {
	return sendto(socket.get(), buf, addr, flags);
}

inline size_t send(Socket const &socket, array_ref<const_buffer_ref> bufs, int flags = 0) {
	return send(socket.get(), bufs, flags);
}

inline size_t send(Socket const &socket, std::initializer_list<const_buffer_ref> bufs, int flags = 0) {
	std::vector<const_buffer_ref> bufs2(bufs);
	return send(socket, ref_array(bufs2), flags);
}

template <typename Addr>
inline size_t sendto(Socket const &socket, const_buffer_ref buf, Addr const &addr, int flags = 0) {
	return sendto(socket.get(), buf, addr, flags);
}

inline void bind(Socket const &socket, const_buffer_ref addr) {
	return bind(socket.get(), addr);
}

template <typename Addr>
inline void bind(Socket const &socket, Addr const &addr) {
	return bind(socket.get(), addr);
}

inline void connect(Socket const &socket, const_buffer_ref addr) {
	return connect(socket.get(), addr);
}

template <typename Addr>
inline void connect(Socket const &socket, Addr const &addr) {
	return connect(socket.get(), addr);
}

} // unistd
} // nz
