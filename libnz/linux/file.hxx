#pragma once
// System
#include <fcntl.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
// Local
#include "common.hxx"
#include "buffer.hxx"

namespace nz {
namespace unistd {

enum class fd_t: int {};
static constexpr fd_t invalid_fd = (fd_t) -1;

/*
       O_APPEND
       O_ASYNC
       O_CLOEXEC (since Linux 2.6.23)
       O_CREAT
       O_DIRECT (since Linux 2.4.10)
       O_DIRECTORY
       O_DSYNC
       O_EXCL Ensure  that this call creates the file: if this flag is specified in conjunction with O_CREAT, and pathname already exists,
       O_LARGEFILE
       O_NOATIME (since Linux 2.6.8)
       O_NOCTTY
       O_NOFOLLOW
       O_NONBLOCK or O_NDELAY
       O_PATH (since Linux 2.6.39)
       O_SYNC Write  operations  on the file will complete according to the requirements of synchronized I/O file integrity completion (by
       O_TMPFILE (since Linux 3.11)
       O_TRUNC
*/

#define NZ_CHECK(call) detail::check((call), __func__)

inline fd_t open(c_string pathname, int flags, mode_t mode) {
	return (fd_t) NZ_CHECK(::open(pathname, flags, mode));
}

inline fd_t openat(fd_t dir, c_string pathname, int flags, mode_t mode) {
	return (fd_t) NZ_CHECK(::openat((int) dir, pathname, flags, mode));
}

inline void close(fd_t fd) {
	NZ_CHECK(::close((int) fd));
}

inline buffer_ref read(fd_t fd, buffer_ref buf) {
	return { buf.data, NZ_CHECK(::read((int) fd, buf.data, buf.size)) };
}

inline size_t write(fd_t fd, const_buffer_ref buf) {
	return NZ_CHECK(::write((int) fd, buf.data, buf.size));
}

#undef NZ_CHECK
} // unistd
} // nz
