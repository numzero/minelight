#pragma once
// System
#include <sys/uio.h>
// Local
#include "array.hxx"
#include "buffer.hxx"
#include "types.hxx"

namespace nz {
namespace unistd {

// make sure `iovec` is compatible with `buffer_ref`  (and `const_buffer_ref`)
static_assert(sizeof(struct iovec) == sizeof(buffer_ref));
static_assert(offsetof(struct iovec, iov_base) == offsetof(buffer_ref, data));
static_assert(offsetof(struct iovec, iov_len) == offsetof(buffer_ref, size));

inline iovec &buffer_to_iovec(buffer_ref &b) {
	return reinterpret_cast<iovec &>(b);
}

inline iovec *array_to_iovec(array_ref<buffer_ref> b) {
	return reinterpret_cast<iovec *>(b.data);
}

inline iovec &const_buffer_to_iovec(const_buffer_ref &b) {
	return reinterpret_cast<iovec &>(b);
}

inline iovec *const_array_to_iovec(array_ref<const_buffer_ref> b) {
	return reinterpret_cast<iovec *>(b.data);
}

} // unistd
} // nz
