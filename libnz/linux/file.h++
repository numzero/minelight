#pragma once
// Local
#include "file.hxx"

namespace nz {
namespace unistd {

class File
{
public:

	File() noexcept = default;
	File(File const &) = delete;
	File(File &&b) noexcept
		: fd(b.release())
	{
	}

	explicit File(fd_t f) noexcept
	{
		reset(f);
	}

	~File()
	{
		reset();
	}

	File &operator= (File const &) noexcept = delete;

	File &operator= (File &&b)
	{
		reset(b.release());
		return *this;
	}

	explicit operator bool() const noexcept
	{
		return fd != invalid_fd;
	}

	fd_t get() const noexcept
	{
		return fd;
	}

	fd_t release() noexcept
	{
		fd_t f = fd;
		fd = invalid_fd;
		return f;
	}

	void reset(fd_t f = invalid_fd)
	{
		std::swap(fd, f);
		if (f != invalid_fd)
			close(f);
	}

private:
	fd_t fd = invalid_fd;
};

inline fd_t openat(File const &dir, c_string pathname, int flags, mode_t mode) {
	return openat(dir.get(), pathname, flags, mode);
}

inline void close(File &file) {
	file.reset();
}

inline buffer_ref read(File const &file, buffer_ref buf) {
	return read(file.get(), buf);
}

inline size_t write(File const &file, const_buffer_ref buf) {
	return write(file.get(), buf);
}

} // unistd
} // nz
