#pragma once
// C
#include <cstring>
// System
#include <sys/types.h>
#include <sys/socket.h>
// Local
#include "common.hxx"
#include "file.hxx"
#include "iov.hxx"

namespace nz {
namespace unistd {

using socket_t = fd_t;

enum class socket_domain: int {
	UNIX = AF_UNIX,
	local = AF_LOCAL,
	inet = AF_INET,
	inet6 = AF_INET6,
	ipx = AF_IPX,
	netlink = AF_NETLINK,
	x25 = AF_X25,
	ax25 = AF_AX25,
	atmpvc = AF_ATMPVC,
	appletalk = AF_APPLETALK,
	packet = AF_PACKET,
	alg = AF_ALG,
};

enum class socket_type: int {
	stream = SOCK_STREAM,
	dgram = SOCK_DGRAM,
	seqpacket = SOCK_SEQPACKET,
	raw = SOCK_RAW,
};

#define NZ_CHECK(call) detail::check((call), __func__)

inline socket_t socket(socket_domain domain, socket_type type, int protocol = 0) {
	return (socket_t) NZ_CHECK(::socket((int) domain, (int) type, protocol));
}

inline buffer_ref recv(socket_t sockfd, buffer_ref buf, int flags = 0) {
	return { buf.data, NZ_CHECK(::recv((int) sockfd, buf.data, buf.size, flags)) };
}

inline std::pair<buffer_ref, buffer_ref> recvfrom(socket_t sockfd, buffer_ref buf, buffer_ref addr_buf, int flags = 0) {
	socklen_t addr_len = addr_buf.size;
	buf.size = NZ_CHECK(::recvfrom((int) sockfd, buf.data, buf.size, flags,
			reinterpret_cast<sockaddr *>(addr_buf.data), &addr_len));
	addr_buf.size = addr_len;
	return { buf, addr_buf };
}

template <typename Addr>
buffer_ref recvfrom(socket_t sockfd, buffer_ref buf, Addr &addr, int flags = 0) {
	socklen_t addr_len = sizeof(addr);
	buf.size = NZ_CHECK(::recvfrom((int) sockfd, buf.data, buf.size, flags,
			reinterpret_cast<sockaddr *>(&addr), &addr_len));
	if (addr_len != sizeof(addr))
		throw std::runtime_error("Got address of a type different from the supplied one");
	return buf;
}

inline size_t send(socket_t sockfd, const_buffer_ref buf, int flags = 0) {
	return NZ_CHECK(::send((int) sockfd, buf.data, buf.size, flags));
}

inline size_t send(socket_t sockfd, array_ref<const_buffer_ref> bufs, int flags = 0) {
	struct msghdr hdr;
	std::memset(&hdr, 0, sizeof(hdr));
	hdr.msg_iov = const_array_to_iovec(bufs);
	hdr.msg_iovlen = bufs.size;
	return NZ_CHECK(::sendmsg((int) sockfd, &hdr, flags));
}

inline size_t sendto(socket_t sockfd, const_buffer_ref buf, const_buffer_ref addr, int flags = 0) {
	return NZ_CHECK(::sendto((int) sockfd, buf.data, buf.size, flags,
			reinterpret_cast<sockaddr const *>(addr.data), addr.size));
}

template <typename Addr>
inline size_t sendto(socket_t sockfd, const_buffer_ref buf, Addr const &addr, int flags = 0) {
	return sendto(sockfd, buf, ref_object(addr), flags);
}

inline void bind(socket_t sockfd, const_buffer_ref addr) {
	NZ_CHECK(::bind((int) sockfd,
			reinterpret_cast<sockaddr const *>(addr.data), addr.size));
}

template <typename Addr>
inline void bind(socket_t sockfd, Addr const &addr) {
	return bind(sockfd, ref_object(addr));
}

inline void connect(socket_t sockfd, const_buffer_ref addr) {
	NZ_CHECK(::connect((int) sockfd,
			reinterpret_cast<sockaddr const *>(addr.data), addr.size));
}

template <typename Addr>
inline void connect(socket_t sockfd, Addr const &addr) {
	return connect(sockfd, ref_object(addr));
}

inline void setsockopt(socket_t sockfd, int level, int optname, const_buffer_ref option) {
	NZ_CHECK(::setsockopt((int) sockfd, level, optname, option.data, option.size));
}

template <typename T>
inline void setsockopt(socket_t sockfd, int level, int optname, T const &option) {
	return setsockopt(sockfd, level, optname, ref_object(option));
}

#undef NZ_CHECK
} // unistd
} // nz
