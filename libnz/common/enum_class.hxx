#pragma once
#include <cstdint>
#include <algorithm>
#include <array>
#include <initializer_list>
#include <string>
#include <stdexcept>
#include <type_traits>

namespace nz {

template <typename _Enum>
struct is_enum_class: std::false_type {};

template <typename _Enum>
static constexpr bool is_enum_class_v = is_enum_class<_Enum>::value;

template <typename _Enum>
struct enum_info;

template <typename _Enum, typename = std::enable_if_t<std::is_enum_v<_Enum>>>
class enum_traits: private enum_info<_Enum> {
	using info = enum_info<_Enum>;
public:
	using type = _Enum;
	using self = enum_traits<type>;
	using underlying = std::underlying_type_t<type>;
	static constexpr auto max = info::count();
};

template <typename _Enum, typename = std::enable_if_t<std::is_enum_v<_Enum>>>
constexpr _Enum enum_cast(typename enum_traits<_Enum>::underlying value)
{
	if (value > enum_traits<_Enum>::max)
		throw std::out_of_range("Enumerator out of range");
	return static_cast<_Enum>(value);
}

template <typename _Enum, typename = std::enable_if_t<std::is_enum_v<_Enum>>>
constexpr std::underlying_type_t<_Enum> enum_cast(_Enum value)
{
	return static_cast<std::underlying_type_t<_Enum>>(value);
}

#define DEFINE_ENUM_CLASS_INFO(UNDERLYING, NAME, ...) \
	template<> struct nz::enum_info<NAME> { \
		static constexpr UNDERLYING count() { \
			enum { __VA_ARGS__ }; \
			return ::std::max({__VA_ARGS__}); \
		} \
	}; \
	template <> struct nz::is_enum_class<NAME>: std::true_type {}

#define enum_class(NAME, ...) \
	enum class NAME { 	__VA_ARGS__ }; \
	DEFINE_ENUM_CLASS_INFO(::std::underlying_type_t<NAME>, NAME, __VA_ARGS__);

#define typed_enum_class(UNDERLYING, NAME, ...) \
	enum class NAME : UNDERLYING { __VA_ARGS__ }; \
	DEFINE_ENUM_CLASS_INFO(UNDERLYING, NAME, __VA_ARGS__);

template <typename Enum, typename Value>
class enum_array {
	static constexpr std::size_t size = static_cast<std::size_t>(enum_traits<Enum>::max) + 1;
	static_assert(size, "Enumeration too large");
	std::array<Value, size> underlying;
public:
	using value_type = Value;
	using key_type = Enum;

	enum_array() = default;
	enum_array(enum_array const &) = default;
	enum_array(std::initializer_list<std::pair<Enum, Value>> const &list) {
		for (auto const &p: list)
			(*this)[p.first] = p.second;
	}

	Value &operator[] (Enum index) {
		return underlying[static_cast<int>(index)];
	}

	Value const &operator[] (Enum index) const {
		return underlying[static_cast<int>(index)];
	}
};

} // nz
