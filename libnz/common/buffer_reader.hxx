#pragma once
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <string>
#include "buffer.hxx"
#include "enum_class.hxx"
#include "types.hxx"

namespace nz {

struct buffer_reader {
	byte const *current;
	byte const *const end;

	buffer_reader() = default;
	buffer_reader(buffer_reader const &) = default;
	buffer_reader(const_buffer_ref buffer)
		: current(buffer.data)
		, end(buffer.data + buffer.size)
	{
	}

	size_t remaining() const
	{
		return end - current;
	}

	const_buffer_ref content() const
	{
		return {current, remaining()};
	}

	void read_be(void *mem, std::size_t size)
	{
#if __BYTE_ORDER == __LITTLE_ENDIAN
		byte const *src = read_nc(size);
		byte *dest = reinterpret_cast<byte *>(mem);
		for (std::size_t k = 0; k < size; k++)
			dest[size - 1 - k] = src[k];
#elif __BYTE_ORDER == __BIG_ENDIAN
		read_raw(mem, size);
#else
#error Invalid endianness
#endif
	}

	void read_raw(void *mem, std::size_t size)
	{
		std::memcpy(mem, read_nc(size), size);
	}

	byte const *read_nc(std::size_t size)
	{
		if (remaining() < size)
			throw std::runtime_error("Buffer underflow");
		auto result = current;
		current += size;
		return result;
	}

	template <typename T, typename = std::enable_if_t<std::is_pod_v<T>>>
	void read(T &object)
	{
		read_be(&object, sizeof(object));
	}

	template <typename T, typename = std::enable_if_t<std::is_pod_v<T>>>
	T read()
	{
		T object;
		read_be(&object, sizeof(object));
		return object;
	}

	template <typename Len, typename Char = char, typename = std::enable_if_t<std::is_integral_v<Len> && std::is_integral_v<Char>>>
	void read_str(std::basic_string<Char> &string)
	{
		Len len;
		read(len);
		string.resize(len);
		read_raw(string.data(), len);
	}

	template <typename Len, typename Char = char, typename = std::enable_if_t<std::is_integral_v<Len> && std::is_integral_v<Char>>>
	std::basic_string<Char> read_str()
	{
		std::basic_string<Char> str;
		Len len;
		read(len);
		str.resize(len);
		read_raw(str.data(), len);
		return str;
	}

	template <typename T, typename = std::enable_if_t<nz::is_enum_class_v<T>>>
	T read_enum()
	{
		return nz::enum_cast<T>(read<std::underlying_type_t<T>>());
	}
};

}
