#pragma once
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <string>
#include "buffer.hxx"
#include "enum_class.hxx"
#include "types.hxx"

namespace nz {

struct buffer_writer {
	byte *current;
	byte *const end;

	buffer_writer() = default;
	buffer_writer(buffer_writer const &) = delete;
	buffer_writer(buffer_ref buffer)
		: current(buffer.data)
		, end(buffer.data + buffer.size)
	{
	}

	size_t remaining() const
	{
		return end - current;
	}

	void write_be(void const *mem, std::size_t size)
	{
#if __BYTE_ORDER == __LITTLE_ENDIAN
		byte const *src = reinterpret_cast<byte const *>(mem);
		byte *dest = write_nc(size);
		for (std::size_t k = 0; k < size; k++)
			dest[size - 1 - k] = src[k];
#elif __BYTE_ORDER == __BIG_ENDIAN
		write_raw({mem, size});
#else
#error Invalid endianness
#endif
	}

	void write_raw(const_buffer_ref buf)
	{
		std::memcpy(write_nc(buf.size), buf.data, buf.size);
	}

	byte *write_nc(std::size_t size)
	{
		if (remaining() < size)
			throw std::runtime_error("Buffer overflow");
		auto result = current;
		current += size;
		return result;
	}

	template <typename T, typename = std::enable_if_t<std::is_pod_v<T>>>
	void write(T const &object)
	{
		write_be(&object, sizeof(object));
	}

	template <typename Len, typename Char = char, typename = std::enable_if_t<std::is_integral_v<Len> && std::is_integral_v<Char>>>
	void write_str(std::basic_string<Char> const &string)
	{
		Len len = string.size();
		write(len);
		write_raw({string.data(), len});
	}

	template <typename Len, typename Item, typename = std::enable_if_t<std::is_integral_v<Len> && std::is_standard_layout_v<Item>>>
	void write_array(nz::array_ref<Item const> array)
	{
		write<Len>(array.size);
		write_raw(array);
	}

	template <typename T, typename = std::enable_if_t<nz::is_enum_class_v<T>>>
	void write_enum(T value)
	{
		write(nz::enum_cast<>(value));
	}
};

}
