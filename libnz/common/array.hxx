#pragma once
#include <vector>
#include "types.hxx"

namespace nz {

template <typename T>
struct array_ref {
	T *data = nullptr;
	size_t size = 0;

	operator array_ref<T const> () {
		return {data, size};
	}

	template <typename U, typename = typename std::enable_if<
			std::is_convertible<T *, U *>::value && (sizeof(T) == sizeof(U))>::type>
	operator array_ref<U> () {
		return {data, size};
	}

	explicit operator bool() const {
		return data;
	}

	array_ref sub(ssize_t begin, size_t length) {
		if (begin < 0)
			begin += size;
		size_t end = size_t(begin) + length;
		if (begin < 0 || begin >= size || end < begin || end > size)
			throw std::out_of_range("Array slice out of range");
		return {data + begin, length};
	}

	array_ref sub(ssize_t begin) {
		if (begin < 0)
			begin += size;
		if (begin < 0 || begin >= size)
			throw std::out_of_range("Array slice out of range");
		return {data + begin, size - begin};
	}
};

template <typename U>
array_ref<U> ref_array(std::vector<U> &array) {
	return { array.data(), array.size() };
}

template <typename U>
array_ref<U const> ref_array(std::vector<U> const &array) {
	return { array.data(), array.size() };
}

template <typename U>
array_ref<U const> ref_array(std::basic_string<U> const &array, bool include_nul = false) {
	return { array.data(), array.size() + include_nul };
}

template <typename U, size_t N>
array_ref<U> ref_array(U (&array)[N]) {
	return { array, N };
}

template <typename T>
T *begin(array_ref<T> const &arr) {
	return arr.data;
}

template <typename T>
T *end(array_ref<T> const &arr) {
	return arr.data + arr.size;
}

template <typename T>
T const *cbegin(array_ref<T> const &arr) {
	return arr.data;
}

template <typename T>
T const *cend(array_ref<T> const &arr) {
	return arr.data + arr.size;
}

} // unistd
