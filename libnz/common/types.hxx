#pragma once
#include <cstdint>
#include <type_traits>

namespace nz {

using ::std::size_t;
using ssize_t = std::make_signed_t<size_t>;
using c_string = char const *;
enum class byte: std::uint8_t {};

}
