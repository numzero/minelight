#pragma once
#include <stdexcept>
#include "array.hxx"
#include "types.hxx"

namespace nz {

using bytearray = std::vector<byte>;

struct buffer_ref;

struct const_buffer_ref: array_ref<byte const> {
	using array_ref::array_ref;

	const_buffer_ref(void const *d, size_t s)
			: array_ref {reinterpret_cast<byte const *>(d), s}
	{
	}

	template <typename T>
	const_buffer_ref(array_ref<T> const &b)
			: const_buffer_ref {b.data, sizeof(T) * b.size}
	{
	}
};

struct buffer_ref: array_ref<byte> {
	using array_ref::array_ref;

	buffer_ref(void *d, size_t s)
			: array_ref {reinterpret_cast<byte *>(d), s}
	{
	}

	template <typename T, typename = typename std::enable_if<
			!std::is_const<T>::value>::type>
	buffer_ref(array_ref<T> const &b)
			: buffer_ref {b.data, sizeof(T) * b.size}
	{
	}
};

template <typename U>
buffer_ref ref_object(U &object) {
	return buffer_ref(&object, sizeof(U));
}

template <typename U>
const_buffer_ref ref_object(U const &object) {
	return const_buffer_ref(&object, sizeof(U));
}

template <typename U>
array_ref<U> as_array(buffer_ref buf) {
	if (buf.size % sizeof(U))
		throw std::invalid_argument("The buffer doens’t represent an array of the type requested");
	// TODO check alignment
	return {reinterpret_cast<U *>(buf.data), buf.size / sizeof(U)};
}

template <typename U>
array_ref<U const> as_array(const_buffer_ref buf) {
	if (buf.size % sizeof(U))
		throw std::invalid_argument("The buffer doens’t represent an array of the type requested");
	// TODO check alignment
	return {reinterpret_cast<U const *>(buf.data), buf.size / sizeof(U)};
}

inline byte *begin(buffer_ref const &buf) {
	return buf.data;
}

inline byte *end(buffer_ref const &buf) {
	return buf.data + buf.size;
}

inline byte const *begin(const_buffer_ref const &buf) {
	return buf.data;
}

inline byte const *end(const_buffer_ref const &buf) {
	return buf.data + buf.size;
}

inline byte const *cbegin(buffer_ref const &buf) {
	return buf.data;
}

inline byte const *cend(buffer_ref const &buf) {
	return buf.data + buf.size;
}

inline byte const *cbegin(const_buffer_ref const &buf) {
	return buf.data;
}

inline byte const *cend(const_buffer_ref const &buf) {
	return buf.data + buf.size;
}

inline bytearray to_array(const_buffer_ref buf) {
	return {begin(buf), end(buf)};
}

}
