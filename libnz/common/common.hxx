#pragma once
#include <system_error>
#include <type_traits>
#include "types.hxx"

namespace nz {
namespace detail {

template <typename T>
std::make_unsigned_t<T> check(T value, char const *fn) {
	if (value < 0)
		throw std::system_error(errno, std::generic_category(), fn);
	return value;
}

} // detail
} // unistd
