#include "udp-connection.hxx"
// C++
#include <chrono>
#include <condition_variable>
#include <limits>
#include <list>
#include <mutex>
// System
#include <signal.h>
#include <unistd.h>
#include <netinet/in.h>
#include <sys/socket.h>
// Library
#include "types.hxx"
#include "buffer_reader.hxx"
#include "buffer_writer.hxx"

using namespace std::literals;

using std::size_t;
using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;

using minetest_magic_t = u32;
static constexpr minetest_magic_t minetest_magic = 0x4f'45'74'03;

class UdpConnection::InboundReliableQueue {
	static constexpr seqnum_t sequence_init = 65500; // MT magic
	static constexpr seqnum_t ring_buffer_size = 1024;
	static constexpr seqnum_t rereceive_tolerance = 256; // make larger if unstable

	static constexpr size_t seqnum_period = (size_t)std::numeric_limits<seqnum_t>::max() + 1;
	static constexpr size_t rereceive_tolerance_inv = seqnum_period - rereceive_tolerance;
	static_assert(seqnum_period % ring_buffer_size == 0, "Ring buffer size must evenly divide full period (seqnum_period, 2^16)");
	static_assert(rereceive_tolerance + ring_buffer_size < 65536, "Ring buffer too large, spurious duplicates may be mistaken as new messages");
public:
	InboundReliableQueue(UdpConnection &connection);
	InboundReliableQueue(InboundReliableQueue const &) = delete;

	InboundPacket *pop_inb_reliable();
	void push_inb_reliable(seqnum_t seq, InboundPacket *msg);

private:
	UdpConnection &connection;
	InboundPacket *inb_reliables[ring_buffer_size];
	seqnum_t inb_reliable_rdptr = sequence_init;
};

UdpConnection::InboundReliableQueue::InboundReliableQueue(UdpConnection &connection)
		: connection(connection)
{
}

void UdpConnection::InboundReliableQueue::push_inb_reliable(seqnum_t seq, InboundPacket *msg)
{
	seqnum_t queue_offset = seq - inb_reliable_rdptr;
	if (queue_offset >= ring_buffer_size) { // xflow
		if (queue_offset >= rereceive_tolerance_inv) { // > -rereceive_tolerance, if treated as signed
			connection.release(msg);
			return; // underflow: we’ve processed previous instance of that packet already
		}
		throw std::runtime_error("Inbound reliable packet ring buffer overflow");
	}
	seqnum_t buffer_offset = seq % ring_buffer_size;
	if (inb_reliables[buffer_offset]) { // we have that message already
		connection.release(msg);
		return;
	}
	inb_reliables[buffer_offset] = msg;
}

InboundPacket *UdpConnection::InboundReliableQueue::pop_inb_reliable()
{
	seqnum_t buffer_offset = inb_reliable_rdptr % ring_buffer_size;
	InboundPacket *msg = inb_reliables[buffer_offset];
	if (msg) {
		inb_reliables[buffer_offset] = nullptr;
		++inb_reliable_rdptr;
	}
	return msg;
}

/**
 * Thread-safe queue for incoming packets
 */
class UdpConnection::InboundPacketQueue {
public:
	InboundPacketQueue() = default;
	InboundPacketQueue(InboundPacketQueue const &) = delete;
	void push(InboundPacket *packet);
	[[nodiscard]] InboundPacket *pop(bool wait);
	[[nodiscard]] InboundPacket *pop(std::chrono::milliseconds timeout);
	void kill();

private:
	std::mutex lock;
	std::condition_variable event;
	std::list<InboundPacket *> queue;
	bool alive = true;
};

void UdpConnection::InboundPacketQueue::push(InboundPacket *packet)
{
	std::lock_guard<std::mutex> g(lock);
	queue.push_back(packet);
	event.notify_one();
}

InboundPacket *UdpConnection::InboundPacketQueue::pop(bool wait)
{
	std::unique_lock<std::mutex> g(lock);
	if (wait)
		while (alive && queue.empty())
			event.wait(g);
	if (queue.empty()) // either `wait ` wasn’t specified, or the queue is dead
		return nullptr;
	InboundPacket *packet = queue.front();
	queue.pop_front();
	return packet;
}

InboundPacket *UdpConnection::InboundPacketQueue::pop(std::chrono::milliseconds timeout)
{
	std::unique_lock<std::mutex> g(lock);
	event.wait_for(g, timeout, [this] () { return !alive || !queue.empty(); });
	if (queue.empty()) // either timed out, or the queue is dead
		return nullptr;
	InboundPacket *packet = queue.front();
	queue.pop_front();
	return packet;
}

void UdpConnection::InboundPacketQueue::kill()
{
	std::lock_guard<std::mutex> g(lock);
	alive = false;
	event.notify_all();
}

using base_clock = std::chrono::steady_clock;
using time_point = base_clock::time_point;

struct OutboundReliablePacket {
	nz::bytearray data;
	time_point resend_at;
	unsigned resend_rem;
};

static constexpr base_clock::duration timeout = 30s;

class UdpConnection::OutboundReliableQueue { // FIXME: support multiple channels
	static constexpr size_t header_size = 11;

	static constexpr seqnum_t sequence_init = 65500; // MT magic
	static constexpr seqnum_t ring_buffer_size = 1024;

	static constexpr size_t seqnum_period = (size_t)std::numeric_limits<seqnum_t>::max() + 1;
	static_assert(seqnum_period % ring_buffer_size == 0, "Ring buffer size must evenly divide full period (seqnum_period, 2^16)");

	static constexpr base_clock::duration resend_interval = 400ms;
	static constexpr unsigned resend_count = timeout / resend_interval;
public:
	OutboundReliableQueue(UdpConnection &connection);
	OutboundReliableQueue(OutboundReliableQueue const &) = delete;

	void push_outb_reliable(channel_id_t channel, nz::const_buffer_ref content, PacketType type = PacketType::Original);
	void ack_outb_reliable(channel_id_t channel, seqnum_t seq);
	bool send_outb_reliable();
	void kill();

private:
	using PacketPtr = std::list<OutboundReliablePacket>::iterator;
	UdpConnection &connection;
	std::mutex lock;
	std::condition_variable event;
	std::list<OutboundReliablePacket> queue;
	PacketPtr outb_reliable_ring[ring_buffer_size];
	seqnum_t outb_reliable_wrptr = sequence_init;
	bool alive = true;
};

UdpConnection::OutboundReliableQueue::OutboundReliableQueue(UdpConnection &connection)
	: connection(connection)
{
}

void UdpConnection::OutboundReliableQueue::push_outb_reliable(channel_id_t channel, nz::const_buffer_ref content, PacketType type)
{
	std::lock_guard<std::mutex> g(lock);
	seqnum_t seq = outb_reliable_wrptr++;
	unsigned pos = seq % ring_buffer_size;
	if (outb_reliable_ring[pos] != PacketPtr())
		throw std::runtime_error("Outbound reliable packet ring buffer overflow");
	queue.emplace_front();
	PacketPtr packet = queue.begin();
	outb_reliable_ring[pos] = packet;
	packet->resend_at = base_clock::now();
	packet->resend_rem = resend_count;
	packet->data.resize(content.size + header_size);
	nz::buffer_writer writer(nz::ref_array(packet->data));
// Minetest header
	writer.write<minetest_magic_t>(minetest_magic);
	writer.write<peer_id_t>(connection.id);
	writer.write<channel_id_t>(channel);
// Reliable header
	writer.write_enum<>(PacketType::Reliable);
	writer.write<seqnum_t>(seq);
// Content
	writer.write_enum<>(type);
	writer.write_raw(content);
	event.notify_one();
}

void UdpConnection::OutboundReliableQueue::ack_outb_reliable(channel_id_t channel, seqnum_t seq)
{
	std::lock_guard<std::mutex> g(lock);
	seqnum_t queue_offset = outb_reliable_wrptr - seq;
	if (queue_offset >= ring_buffer_size)
		throw ProtocolError("Spurious ack received");
	seqnum_t buffer_offset = seq % ring_buffer_size;
	PacketPtr packet = outb_reliable_ring[buffer_offset];
	if (packet == PacketPtr())
		throw ProtocolError("Spurious ack received");
	outb_reliable_ring[buffer_offset] = PacketPtr();
	queue.erase(packet);
}

bool UdpConnection::OutboundReliableQueue::send_outb_reliable()
{
	std::unique_lock<std::mutex> g(lock);
	PacketPtr packet;
	for (;;) {
		if (!alive)
			return false;
		if (queue.empty()) {
			event.wait(g);
			continue;
		}
		packet = queue.begin();
		time_point now = base_clock::now();
		if (now >= packet->resend_at)
			break;
		event.wait_until(g, packet->resend_at);
		// Loop here, as the packed could be acked and removed.
		// Or a new packed might be queued.
	}
	if (!packet->resend_rem)
		throw ProtocolError("Connection timed out");
	packet->resend_rem--;
	packet->resend_at = base_clock::now() + resend_interval;
	queue.splice(queue.end(), queue, packet);
	nz::unistd::send(connection.socket, ref_array(packet->data), 0); // FIXME: move this out of lock
	return true;
}

void UdpConnection::OutboundReliableQueue::kill()
{
	std::lock_guard<std::mutex> g(lock);
	alive = false;
	event.notify_all();
}

class UdpConnection::Channel {
public:
	UdpConnection &connection;
	channel_id_t const id;

	Channel(UdpConnection &connection, channel_id_t id);
	void process_packet(InboundPacket *packet);

private:
	static nz::enum_array<PacketType, void (UdpConnection::Channel::*)(InboundPacket *packet)> packet_type_handlers;
	InboundReliableQueue reliable_queue;

	void process_control_packet(InboundPacket *packet);
	void enqueue_packet(InboundPacket *packet);
	void process_packet_part(InboundPacket *packet);
	void process_reliable_packet(InboundPacket *packet);

	void process_pending_reliables();
};

nz::enum_array<PacketType, void (UdpConnection::Channel::*)(InboundPacket *packet)>
	UdpConnection::Channel::packet_type_handlers = {
		{ PacketType::Control, &UdpConnection::Channel::process_control_packet },
		{ PacketType::Original, &UdpConnection::Channel::enqueue_packet },
		{ PacketType::Split, &UdpConnection::Channel::process_packet_part },
		{ PacketType::Reliable, &UdpConnection::Channel::process_reliable_packet },
};

UdpConnection::Channel::Channel(UdpConnection& connection, channel_id_t id)
		: connection(connection)
		, id(id)
		, reliable_queue(connection)
{
}

void UdpConnection::Channel::process_packet(InboundPacket *packet)
{
	PacketType type = packet->read_enum<PacketType>();
	(this->*packet_type_handlers[type])(packet);
}

void UdpConnection::Channel::process_control_packet(InboundPacket* packet)
{
	ControlType type = packet->read_enum<ControlType>();
	switch (type) {
		case ControlType::Ack:
			connection.outb_queue->ack_outb_reliable(id, packet->read<seqnum_t>());
			break;
		case ControlType::SetPeerId:
			connection.set_peer_id(packet->read<peer_id_t>());
			break;
		case ControlType::Ping:
			return;
		case ControlType::Disconnect:
			throw Disconnect(); // release will be called by the connection itself
	}
	connection.release(packet);
}

void UdpConnection::Channel::enqueue_packet(InboundPacket *packet)
{
	connection.queue->push(packet);
}

void UdpConnection::Channel::process_packet_part(InboundPacket *packet)
{
	// TODO: process split packets
}

void UdpConnection::Channel::process_reliable_packet(InboundPacket *packet)
{
	auto seq = packet->read<seqnum_t>();
	connection.send_ack(id, seq);
	reliable_queue.push_inb_reliable(seq, packet);
	process_pending_reliables();
}

void UdpConnection::Channel::process_pending_reliables()
{
	InboundPacket *packet;
	while ((packet = reliable_queue.pop_inb_reliable())) {
		PacketType type = packet->read_enum<PacketType>();
		if (type == PacketType::Reliable)
			throw ProtocolError("Nested reliable received");
		(this->*packet_type_handlers[type])(packet);
	}
}

UdpConnection::UdpConnection(int stop_signal)
		: buffer_pool(32, 1024)
		, stop_signal(stop_signal)
{
	queue.reset(new InboundPacketQueue());
	outb_queue.reset(new OutboundReliableQueue(*this));
	for (int k = 0; k < channel_count; k++)
		channels[k].reset(new Channel(*this, k));
}

UdpConnection::~UdpConnection()
{
	stop();
	receive_thread.join();
	send_thread.join();
}

void UdpConnection::receive_all()
{
	for (;;) {
		InboundPacket *packet;
		try {
			packet = receive_single();
		} catch (std::system_error &e) {
			if (e.code().category() != std::generic_category())
				throw;
			switch (e.code().value()) {
#if EWOULDBLOCK != EAGAIN
				case EWOULDBLOCK:
#endif
				case EAGAIN:
				case EINTR:
					return;
				default:
					throw;
			}
		}
		if (!packet)
			continue;
		try {
			process_network_packet(packet);
		} catch (std::exception &) {
			release(packet);
			throw;
		}
	}
}

void UdpConnection::start()
{
// Unblock stop_signal for this thread, create new threads
// which will inherit that, then restore the signal mask.
	sigset_t own_sigmask;
	sigset_t signals;
	sigemptyset(&signals);
	sigaddset(&signals, stop_signal);
	pthread_sigmask(SIG_UNBLOCK, &signals, &own_sigmask);
	receive_thread = std::thread(&UdpConnection::receive_all, this);
	send_thread = std::thread(&UdpConnection::send_all, this);
	pthread_sigmask(SIG_SETMASK, &own_sigmask, nullptr);
}

void UdpConnection::stop()
{
	queue->kill();
	outb_queue->kill();
	// unblock the socket, then interrupt the receive, if in progress
	::fcntl((int) socket.get(), F_SETFL, O_NONBLOCK);
	pthread_kill(receive_thread.native_handle(), stop_signal);
	pthread_kill(send_thread.native_handle(), stop_signal);
}

void UdpConnection::connect(std::uint32_t addr, std::uint16_t port)
{
	nz::unistd::Socket fd{nz::unistd::socket(nz::unistd::socket_domain::inet, nz::unistd::socket_type::dgram)};

	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;

	address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_port = htons(0);
	nz::unistd::bind(fd, address);

	address.sin_addr.s_addr = htonl(addr);
	address.sin_port = htons(port);
	nz::unistd::connect(fd, address);

	socket = std::move(fd);
	remote_addr = addr;
	remote_port = port;
}

InboundPacket *UdpConnection::receive()
{
	return queue->pop(true);
}

InboundPacket *UdpConnection::receive(std::chrono::milliseconds timeout)
{
	return queue->pop(timeout);
}

void UdpConnection::send(nz::const_buffer_ref content, bool reliable, channel_id_t channel)
{
	if (reliable)
		outb_queue->push_outb_reliable(channel, content);
	else
		send_single(content, channel);
}

void UdpConnection::release(InboundPacket *message)
{
	message->~InboundPacket();
	release_buffer(message);
}

void UdpConnection::release_buffer(void *buffer)
{
	buffer_pool.release_buffer(buffer);
}

InboundPacket *UdpConnection::receive_single()
{
	nz::buffer_ref buffer = buffer_pool.take_buffer();
	if (!buffer)
		throw std::runtime_error("Can't allocate buffer for new packet");
	try {
		auto body = buffer.sub(sizeof(InboundPacket));
		sockaddr_in remote;
		auto packet = recvfrom(socket, body, remote);
		u32 addr = ntohl(remote.sin_addr.s_addr);
		u16 port = ntohs(remote.sin_port);
		if ((addr != remote_addr) || (port != remote_port)) {
			release_buffer(buffer.data);
			return nullptr;
		}
		return new(buffer.data) InboundPacket {{packet}};
	} catch (...) {
		release_buffer(buffer.data);
		throw;
	}
}

void UdpConnection::process_network_packet(InboundPacket *packet)
{
	auto magic = packet->read<minetest_magic_t>();
	auto sender = packet->read<peer_id_t>();
	auto channel_id = packet->read<channel_id_t>();
	if (magic != minetest_magic)
		throw ProtocolError("Not a Minetest packet");
	if (sender != remote_id)
		throw ProtocolError("Invalid remote peer ID");
	auto const &channel = channels.at(channel_id);
	channel->process_packet(packet);
}

void UdpConnection::set_peer_id(peer_id_t new_peer_id)
{
	if (id && id != new_peer_id)
		throw ProtocolError("Peer ID already set");
	id = new_peer_id; // FIXME: non-thread-safe
}

void UdpConnection::send_ack(channel_id_t channel, seqnum_t seq)
{
	nz::byte packet[11];
	nz::buffer_writer writer(nz::ref_array(packet));
// Minetest header
	writer.write<minetest_magic_t>(minetest_magic);
	writer.write<peer_id_t>(id);
	writer.write<channel_id_t>(channel);
// Reliable header
	writer.write_enum<PacketType>(PacketType::Control);
	writer.write_enum<ControlType>(ControlType::Ack);
	writer.write<seqnum_t>(seq);
	nz::unistd::send(socket, ref_array(packet), 0);
}

void UdpConnection::send_all()
{
	while (outb_queue->send_outb_reliable())
		;
}

void UdpConnection::send_single(nz::const_buffer_ref content, channel_id_t channel)
{
	nz::byte header[8];
	nz::buffer_writer writer(nz::ref_array(header));
// Minetest header
	writer.write<minetest_magic_t>(minetest_magic);
	writer.write<peer_id_t>(id);
	writer.write<channel_id_t>(channel);
// Unreliable header
	writer.write_enum<PacketType>(PacketType::Original);
	nz::unistd::send(socket, {ref_array(header), content}, 0);
}
