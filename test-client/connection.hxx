#pragma once
// C
#include <cstdint>
// C++
#include <stdexcept>
// Library
#include "types.hxx"
#include "buffer.hxx"
#include "buffer_reader.hxx"

using channel_id_t = std::uint8_t;

struct InboundPacket: nz::buffer_reader {
};

class Connection {
public:
	struct Disconnect {};
	struct ProtocolError: public std::runtime_error { using runtime_error::runtime_error; };

	virtual ~Connection() = default;

	/**
	 * Receive single message
	 *  @returns Message body
	 *  @throws @c Disconnect
	 *
	 * Receives a message. Processes pending auxiliary messages, if any.
	 * The packet storage must be returned using @c release.
	 */
	virtual InboundPacket *receive() = 0;

	virtual void release(InboundPacket *message) = 0;

	/**
	 * Send single message
	 * @param content Message body
	 * @param reliable Whether to send the message as “reliable”
	 * @param channel Subchannel to use; higher numbers are lower priority
	 *
	 * Sends a message, splitting it if necessary. Channel number is important
	 * for “reliable” messages, which are ordered in each channel but
	 * not across ones.
	 */
	virtual void send(nz::const_buffer_ref content, bool reliable = false, channel_id_t channel = 0) = 0;
};
