// C++
#include <chrono>
#include <unordered_map>
// System
#include <signal.h>
#include <netinet/in.h>
// Library
#include <fmt/printf.h>
// Local
#include "../import/srp.h"
#include "buffer_writer.hxx"
#include "connection.hxx"
#include "debug.hxx"
#include "udp-connection.hxx"

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using u64 = std::uint64_t;
using s8 = std::int8_t;
using s16 = std::int16_t;
using s32 = std::int32_t;
using s64 = std::int64_t;
using command_t = u16;
using bytestring = std::basic_string<u8>;

using namespace std::literals;

void make_connect_message(nz::buffer_writer &writer)
{
	writer.write<command_t>(0x0000);
}

void make_init_message(nz::buffer_writer &writer, std::string const &player_name)
{
	writer.write<command_t>(0x0002);
	writer.write<u8>(28); // serialization version
	writer.write<u16>(0); // compression
	writer.write<u16>(37); // min. protocol version
	writer.write<u16>(37); // max. protocol version
	writer.write_str<u16, char>(player_name);
}

void make_init2_message(nz::buffer_writer &writer)
{
	writer.write<command_t>(0x0011);
}

void make_ready_message(nz::buffer_writer &writer)
{
	writer.write<command_t>(0x0043);
	writer.write<u8>(0);
	writer.write<u8>(5);
	writer.write<u8>(0);
	writer.write<u8>(1);
	writer.write_str<u16, char>("Minelight 0.5.0-compatible");
}

void make_srp_a_message(nz::buffer_writer &writer, nz::const_buffer_ref a, bool legacy = false)
{
	writer.write<command_t>(0x0051);
	writer.write_array<u16>(a);
	writer.write<u8>(legacy ? 0 : 1);
}

void make_srp_m_message(nz::buffer_writer &writer, nz::const_buffer_ref m)
{
	writer.write<command_t>(0x0052);
	writer.write_array<u16>(m);
}

UdpConnection con(SIGRTMIN + 2);
nz::bytearray buf(1024);

template <typename ...Args>
void send_message(void (*builder)(nz::buffer_writer &writer, Args ...args), bool reliable, decltype(std::declval<Args>()) &&...args)
{
	nz::buffer_writer writer(nz::ref_array(buf));
	builder(writer, std::forward<Args>(args)...);
	nz::const_buffer_ref packet {buf.data(), size_t(writer.current - buf.data())};
	con.send(packet, reliable);
}

template <typename ...Args>
void send_message_c(void (*builder)(nz::buffer_writer &writer, Args ...args), bool reliable, channel_id_t channel, decltype(std::declval<Args>()) &&...args)
{
	nz::buffer_writer writer(nz::ref_array(buf));
	builder(writer, std::forward<Args>(args)...);
	nz::const_buffer_ref packet {buf.data(), size_t(writer.current - buf.data())};
	con.send(packet, reliable, channel);
}

struct toclient_hello
{
	static constexpr command_t command = 0x0002;
	u8 serialization_version;
	u16 compression_type;
	u16 protocol_version;
	u32 allowed_auth;
	std::string lowercase_name;
};

toclient_hello parse_hello_message(nz::buffer_reader &reader)
{
	toclient_hello result;
	result.serialization_version = reader.read<u8>();
	result.compression_type = reader.read<u16>();
	result.protocol_version = reader.read<u16>();
	result.allowed_auth = reader.read<u32>();
	result.lowercase_name = reader.read_str<u16, char>();
	return result;
}

struct toclient_srp_b
{
	static constexpr command_t command = 0x0060;
	bytestring s;
	bytestring B;
};

toclient_srp_b parse_srp_b_message(nz::buffer_reader &reader)
{
	toclient_srp_b result;
	result.s = reader.read_str<u16, u8>();
	result.B = reader.read_str<u16, u8>();
	return result;
}

struct toclient_access_granted
{
	static constexpr command_t command = 0x0003;
	float x, y, z;
	u64 map_seed;
	std::chrono::duration<float> send_interval;
	u32 sudo_auth;
};

toclient_access_granted parse_access_granted_message(nz::buffer_reader &reader)
{
	toclient_access_granted result;
	result.x = reader.read<float>();
	result.y = reader.read<float>();
	result.z = reader.read<float>();
	result.map_seed = reader.read<u64>();
	result.send_interval = std::chrono::duration<float>(reader.read<float>());
	result.sudo_auth = reader.read<u32>();
	return result;
}

struct toclient_access_denied
{
	static constexpr command_t command = 0x000a;
	u8 reason;
	bool reconnect;
	std::string reason_str;
};

toclient_access_denied parse_access_denied_message(nz::buffer_reader &reader)
{
	toclient_access_denied result;
	result.reason = reader.read<u8>();
	result.reason_str = reader.read_str<u16>();
	result.reconnect = reader.read<u8>();
	return result;
}

template <typename Result>
Result parse_message(InboundPacket &packet, Result (*parser)(nz::buffer_reader &reader))
{
	if (packet.read<command_t>() != Result::command)
		throw UdpConnection::ProtocolError("Unexpected command");
	return parser(packet);
}

template <typename Result>
Result parse_message(InboundPacketPtr &packet, Result (*parser)(nz::buffer_reader &reader))
{
	if (packet->read<command_t>() != Result::command)
		throw UdpConnection::ProtocolError("Unexpected command");
	Result result = parser(*packet);
	packet = nullptr;
	return result;
}

void auth_srp(std::string const &name, std::string const &lc_name, std::string const &password)
{
	fmt::printf("Performing SRP auth\n");
	auto pwd = nz::as_array<u8 const>(nz::ref_array(password));
	SRPUser *srp = srp_user_new(SRP_SHA256, SRP_NG_2048,
				name.c_str(), lc_name.c_str(),
				pwd.data, pwd.size, nullptr, nullptr);

	nz::array_ref<u8> a;
	SRP_Result res = srp_user_start_authentication(srp, nullptr, nullptr, 0, &a.data, &a.size);
	if (res != SRP_OK)
		abort();
	fmt::printf("Sending SRP auth\n");
	send_message_c(make_srp_a_message, true, 1, a, false);

	InboundPacketPtr reply{con, con.receive()};
	auto srpb = parse_message(reply, parse_srp_b_message);
	nz::array_ref<u8> m;
	srp_user_process_challenge(srp,
		srpb.s.data(), srpb.s.size(),
		srpb.B.data(), srpb.B.size(),
		&m.data, &m.size);
	if (!m)
		abort();
	send_message_c(make_srp_m_message, true, 1, m);

	reply = con.receive();
	auto command = reply->read<command_t>();
	switch (command) {
		case toclient_access_granted::command: {
			auto ag = parse_access_granted_message(*reply);
			fmt::printf("Access granted! You're at (%.1f, %.1f, %.1f), map seed %ld, send interval %.3f, sudo %#b\n",
					ag.x, ag.y, ag.z, ag.map_seed, ag.send_interval.count(), ag.sudo_auth);
			break;
		}
		case toclient_access_denied::command: {
			auto ad = parse_access_denied_message(*reply);
			fmt::printf("Access denied %s reconnection for reason %d \"%s\"\n",
					ad.reconnect ? "with" : "without", ad.reason, ad.reason_str);
			throw UdpConnection::Disconnect();
		}
	}
}

int main(int argc, char **argv)
{
	con.connect(INADDR_LOOPBACK, 30000);
	con.start();
	fmt::printf("Connecting...\n");
	send_message(make_connect_message, false);
	InboundPacketPtr msg{con};
	auto name = "numzero"s;
	auto password = ""s;
	do {
		fmt::printf("Sending init\n");
		send_message(make_init_message, false, name);
		msg = con.receive(100ms);
	} while (!msg);
	auto hello = parse_message(*msg, parse_hello_message);
	auth_srp(name, hello.lowercase_name, password);
	send_message(make_init2_message, false);
	send_message(make_ready_message, false);
	InboundPacketPtr reply{con};
	while ((reply = con.receive())) {
		fmt::printf("Got reply: %s\n", hexify(reply->content()));
	}
	return 0;
}
