#pragma once
#include <cstdint>
#include <atomic>
#include <memory>
#include "buffer.hxx"

#if !defined(DEBUG_BUFFER_FLIGHT) && !defined(NDEBUG)
#define DEBUG_BUFFER_FLIGHT 1
#endif

/**  Fast way to allocate fixed-size chunks of memory
 */
class BufferPool {
public:
	BufferPool(std::size_t buffer_count, std::size_t buffer_size = 2048);
	BufferPool(BufferPool const &) = delete;
	BufferPool(BufferPool &&) = delete;
	~BufferPool();

	[[nodiscard]] nz::buffer_ref take_buffer();
	void release_buffer(void *buf);
	void release_buffer(nz::buffer_ref buf);
	bool is_own_buffer(void const *buf) const;

private:
	struct ChainedBuffer;
	std::atomic<ChainedBuffer *> head;
	std::atomic<ChainedBuffer *> tail;
#if DEBUG_BUFFER_FLIGHT
	std::atomic<std::size_t> buffers_in_flight = {0};
#endif
	std::size_t const buffer_size;
	std::size_t const memory_size;
	std::unique_ptr<nz::byte[]> const memory;
};
