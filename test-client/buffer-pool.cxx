#include "buffer-pool.hxx"
#include <stdexcept>

struct BufferPool::ChainedBuffer {
	std::atomic<ChainedBuffer *> next;
};

BufferPool::BufferPool(std::size_t buffer_count, std::size_t buffer_size)
	: buffer_size(buffer_size)
	, memory_size(buffer_count * buffer_size)
	, memory(new nz::byte[memory_size])
{
	nz::byte *region = memory.get();
	auto get_buffer = [&] (int k) -> ChainedBuffer * {
		return reinterpret_cast<ChainedBuffer *>(region + k * buffer_size);
	};
	for (int k = 0; k < buffer_count; k++) {
		auto buffer = get_buffer(k);
		new (buffer) ChainedBuffer { get_buffer(k + 1) };
	}
	get_buffer(buffer_count - 1)->next = nullptr;
	head.store(get_buffer(0));
	tail.store(get_buffer(buffer_count - 1));
}

BufferPool::~BufferPool()
{
#if DEBUG_BUFFER_FLIGHT
	if (buffers_in_flight)
		throw std::logic_error("Buffer pool is destroyed while being used");
#endif
	// Make C++ happy. The dtor does nothing, so that should be optimized out entirely
	for (int offset = 0; offset < memory_size; offset += buffer_size)
		reinterpret_cast<ChainedBuffer *>(memory.get() + offset)->~ChainedBuffer();
}

nz::buffer_ref BufferPool::take_buffer()
{
	ChainedBuffer *buffer = head.load();
	for (;;) {
		ChainedBuffer *const next = buffer->next;
		if (!next) // exactly: we can’t touch that buffer since `tail` points to it
			return {};
		if (head.compare_exchange_weak(buffer, next))
			break;
	}
	buffer->~ChainedBuffer(); // make C++ happy
#if DEBUG_BUFFER_FLIGHT
	++buffers_in_flight;
#endif
	return {buffer, buffer_size};
}

void BufferPool::release_buffer(void *buf)
{
#if DEBUG_BUFFER_FLIGHT
	if (!is_own_buffer(buf))
		throw std::logic_error("Buffer is returned to a wrong pool");
	--buffers_in_flight;
#endif
	auto const buffer = new (buf) ChainedBuffer;
	auto const old_tail = tail.exchange(buffer);
	old_tail->next = buffer;
}

void BufferPool::release_buffer(nz::buffer_ref buf)
{
#if DEBUG_BUFFER_FLIGHT
	if (buf.size != buffer_size)
		throw std::logic_error("Invalid buffer supplied to release_buffer(buffer_ref)");
#endif
	release_buffer(buf.data);
}

bool BufferPool::is_own_buffer(void const *buf) const
{
	std::ptrdiff_t offset = reinterpret_cast<nz::byte const *>(buf) - memory.get();
	if (offset < 0 || offset >= memory_size)
		return false;
#if DEBUG_BUFFER_FLIGHT
	if (offset % buffer_size)
		throw std::logic_error("Invalid pointer supplied to is_own_buffer");
#endif
	return true;
}
