#pragma once
// C
#include <cstdint>
// C++
#include <array>
#include <atomic>
#include <chrono>
#include <memory>
#include <stdexcept>
#include <thread>
#include <vector>
// Library
#include "buffer.hxx"
#include "buffer_reader.hxx"
#include "enum_class.hxx"
#include "linux/socket.h++"
// Local
#include "buffer-pool.hxx"
#include "connection.hxx"

typed_enum_class(std::uint8_t, PacketType,
	Control,
	Original,
	Split,
	Reliable,
);

typed_enum_class(std::uint8_t, ControlType,
	Ack,
	SetPeerId,
	Ping,
	Disconnect,
);

using peer_id_t = std::uint16_t;
using seqnum_t = std::uint16_t;

class UdpConnection: public Connection {
public:
	static constexpr int channel_count = 3;

	UdpConnection(int stop_signal);
	~UdpConnection();

	void connect(std::uint32_t addr, std::uint16_t port);

	[[nodiscard]] InboundPacket *receive() override;
	[[nodiscard]] InboundPacket *receive(std::chrono::milliseconds timeout);
	void release(InboundPacket *message) override;

	void send(nz::const_buffer_ref content, bool reliable = false, channel_id_t channel = 0) override;

	void start();
	void stop();
/*
	message_h begin_message(nz::buffer_ref &area);
	reliable_h begin_reliable(nz::buffer_ref &area);
	void send(message_h message);
	void send(reliable_h message);
*/
private:
	class InboundReliableQueue;
	class InboundPacketQueue;
	class OutboundReliableQueue;
	class Channel;

	std::uint32_t remote_addr;
	std::uint16_t remote_port;
	peer_id_t id = 0;
	peer_id_t remote_id = 1;
	std::uint16_t send_reliable_seq = 0;
	std::uint16_t send_split_seq = 0;

	BufferPool buffer_pool;
	std::array<std::unique_ptr<Channel>, channel_count> channels;
	std::unique_ptr<InboundPacketQueue> queue;
	std::unique_ptr<OutboundReliableQueue> outb_queue;
	nz::unistd::Socket socket;

	int stop_signal;
	std::atomic<bool> alive = {true};
	std::thread receive_thread;
	std::thread send_thread;

	void set_peer_id(peer_id_t new_peer_id);
	void send_ack(channel_id_t channel, seqnum_t seq);

	void release_buffer(void *buffer);
	void receive_all();
	[[nodiscard]] InboundPacket *receive_single();
	void process_network_packet(InboundPacket *packet);
	void send_all();
	void send_single(nz::const_buffer_ref content, channel_id_t channel = 0);
};

struct InboundPacketPtr {
	UdpConnection &connection;
	InboundPacket *packet = nullptr;

	~InboundPacketPtr() {
		if (packet)
			connection.release(packet);
	}

	InboundPacketPtr &operator= (InboundPacket *_packet) {
		if (packet)
			connection.release(packet);
		packet = _packet;
		return *this;
	}

	InboundPacket &operator* () {
		return *packet;
	}

	InboundPacket *operator-> () {
		return packet;
	}

	explicit operator bool () {
		return packet;
	}
};
