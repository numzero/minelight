#pragma once
#include <cstdint>
#include <stdexcept>

using ubyte = std::uint8_t;

template <int _bytes>
struct UIntBE {
	using self = UIntBE;
	using base_type = unsigned long long;
	static constexpr int bytes = _bytes;
	static constexpr base_type limit = (1ull << (8 * bytes)) - 1; // will overflow correctly if _bytes == sizeof(base_type)
	static_assert(sizeof(ubyte) == sizeof(char));
	static_assert(bytes > 0);
	static_assert(bytes <= sizeof(base_type));

	UIntBE() = default;
	UIntBE(UIntBE const &) = default;
	UIntBE(base_type value) {
		set(value);
	}

	base_type get() const {
		base_type value;
		auto dest = reinterpret_cast<ubyte *>(&value);
		for (int k = 0; k < bytes; k++)
			dest[k] = data[offset(k)];
		return value;
	}

	void set(base_type value) {
		if (value > limit)
			throw std::out_of_range("Value not representable");
		auto src = reinterpret_cast<ubyte const *>(&value);
		for (int k = 0; k < bytes; k++)
			data[offset(k)] = src[k];
	}

	UIntBE & operator= (UIntBE const &) = default;

	base_type operator= (base_type value) {
		set(value);
		return value;
	}

	operator base_type () const noexcept {
		return get();
	}

private:
	ubyte data[bytes];

#ifdef __BYTE_ORDER
#if __BYTE_ORDER == __LITTLE_ENDIAN
	static int offset(int native) {
		return bytes - 1 - native;
	}
#elif __BYTE_ORDER == __BIG_ENDIAN
	static int offset(int native) {
		return native;
	}
#endif
#endif
};

using uint8_be = std::uint8_t; // endian-independent
using uint16_be = UIntBE<2>;
using uint32_be = UIntBE<4>;
using uint64_be = UIntBE<8>;
