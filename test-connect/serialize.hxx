#pragma once
#include <cstdint>
#include <cstring>
#include <stdexcept>
#include <string>
#include <vector>
#include "types.hxx"

using byte = nz::byte;
using bytearray = std::vector<byte>;

using namespace std::literals;

struct buffer_iterator {
	bytearray &buffer;
	std::size_t pos = 0;

	void read_be(void *mem, std::size_t size)
	{
#if __BYTE_ORDER == __LITTLE_ENDIAN
		std::size_t base = advance(size, "Buffer underflow"s);
		byte *dest = reinterpret_cast<byte *>(mem);
		for (std::size_t k = 0; k < size; k++)
			dest[size - 1 - k] = buffer[base + k];
#elif __BYTE_ORDER == __BIG_ENDIAN
		read_raw(mem, size);
#else
#error Invalid endianness
#endif
	}

	void read_raw(void *mem, std::size_t size)
	{
		std::size_t base = advance(size, "Buffer underflow"s);
		std::memcpy(mem, &buffer[base], size);
	}

	void write_be(void const *mem, std::size_t size)
	{
#if __BYTE_ORDER == __LITTLE_ENDIAN
		std::size_t base = advance(size, "Buffer overflow"s);
		byte const *src = reinterpret_cast<byte const *>(mem);
		for (std::size_t k = 0; k < size; k++)
			buffer[base + k] = src[size - 1 - k];
#elif __BYTE_ORDER == __BIG_ENDIAN
		write_raw(mem, size);
#else
#error Invalid endianness
#endif
	}

	void write_raw(void const *mem, std::size_t size)
	{
		std::size_t base = advance(size, "Buffer overflow"s);
		std::memcpy(&buffer[base], mem, size);
	}

	/// @returns old position
	/// @throws @c std::runtime_error
	std::size_t advance(std::size_t by, std::string const &error_message)
	{
		std::size_t npos = pos + by;
		if (npos < pos || npos > buffer.size())
			throw std::runtime_error(error_message);
		std::swap(pos, npos);
		return npos;
	}

	template <typename T, typename = std::enable_if_t<std::is_pod_v<T>>>
	void read(T &object)
	{
		read_be(&object, sizeof(object));
	}

	template <typename T, typename = std::enable_if_t<std::is_pod_v<T>>>
	T read()
	{
		T object;
		read_be(&object, sizeof(object));
		return object;
	}

	template <typename T, typename = std::enable_if_t<std::is_pod_v<T>>>
	void write(T const &object)
	{
		write_be(&object, sizeof(object));
	}

	template <typename Len, typename Char = char, typename = std::enable_if_t<std::is_integral_v<Len> && std::is_integral_v<Char>>>
	void read_str(std::basic_string<Char> &string)
	{
		Len len;
		read(len);
		string.resize(len);
		read_raw(string.data(), len);
	}

	template <typename Len, typename Char, typename = std::enable_if_t<std::is_integral_v<Len> && std::is_integral_v<Char>>>
	std::basic_string<Char> read_str()
	{
		std::basic_string<Char> str;
		Len len;
		read(len);
		str.resize(len);
		read_raw(str.data(), len);
		return str;
	}

	template <typename Len, typename Char, typename = std::enable_if_t<std::is_integral_v<Len> && std::is_integral_v<Char>>>
	void write_str(std::basic_string<Char> &string)
	{
		Len len = string.size();
		if (len != string.size())
			throw std::runtime_error("String too long");
		write(len);
		write_raw(string.data(), len);
	}
};
