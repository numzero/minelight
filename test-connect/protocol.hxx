#pragma once
#include <cstdint>
#include "types.hxx"
#include "serialize.hxx"

enum class ToClientCommand {
	Hello = 0x02,
	AuthAccept = 0x03,
	AcceptSudoMode = 0x04,
	DenySudoMode = 0x05,
	AccessDenied = 0x0a,
	Blockdata = 0x20,
	Addnode = 0x21,
	Removenode = 0x22,
	Inventory = 0x27,
	TimeOfDay = 0x29,
	CsmRestrictionFlags = 0x2a,
	ChatMessage = 0x2f,
	ActiveObjectRemoveAdd = 0x31,
	ActiveObjectMessages = 0x32,
	HP = 0x33,
	MovePlayer = 0x34,
	AccessDeniedLegacy = 0x35,
	Deathscreen = 0x37,
	Media = 0x38,
	Tooldef = 0x39,
	Nodedef = 0x3a,
	Craftitemdef = 0x3b,
	AnnounceMedia = 0x3c,
	Itemdef = 0x3d,
	PlaySound = 0x3f,
	StopSound = 0x40,
	Privileges = 0x41,
	InventoryFormspec = 0x42,
	DetachedInventory = 0x43,
	ShowFormspec = 0x44,
	Movement = 0x45,
	SpawnParticle = 0x46,
	AddParticlespawner = 0x47,
	Hudadd = 0x49,
	Hudrm = 0x4a,
	Hudchange = 0x4b,
	HudSetFlags = 0x4c,
	HudSetParam = 0x4d,
	Breath = 0x4e,
	SetSky = 0x4f,
	OverrideDayNightRatio = 0x50,
	LocalPlayerAnimations = 0x51,
	EyeOffset = 0x52,
	DeleteParticlespawner = 0x53,
	CloudParams = 0x54,
	FadeSound = 0x55,
	UpdatePlayerList = 0x56,
	ModchannelMsg = 0x57,
	ModchannelSignal = 0x58,
	SrpBytesSB = 0x60,
	FormspecPrepend = 0x61,
};

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;

struct ts_init {
	int ser_version = 28;
	int compression = 0;
	int min_version = 37;
	int max_version = 37;
	std::string player_name;

	void parse(bytearray &buf) {
		buffer_iterator it = { buf };
		it.read<u16>();
		ser_version = it.read<u8>();
		compression = it.read<u16>();
		min_version = it.read<u16>();
		max_version = it.read<u16>();
		player_name = it.read_str<u16, char>();
	}

	nz::buffer_ref serialize(bytearray &buf) {
		buffer_iterator it = { buf };
		it.write<u16>(0x0002);
		it.write<u8>(ser_version);
		it.write<u16>(compression);
		it.write<u16>(min_version);
		it.write<u16>(max_version);
		it.write_str<u16, char>(player_name);
		return { buf.data(), it.pos };
	}
};

struct ts_init2 {
	void parse(bytearray &buf) {
		buffer_iterator it = { buf };
		it.read<u16>();
		it.read<u16>();
	}

	nz::buffer_ref serialize(bytearray &buf) {
		buffer_iterator it = { buf };
		it.write<u16>(0x0011);
		it.write<u16>(0);
		return { buf.data(), it.pos };
	}
};
