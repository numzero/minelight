// C++
#include <atomic>
#include <array>
#include <thread>
// System
#include <unistd.h>
// Library
#include <fmt/printf.h>
// Local
#include "binary.hxx"
#include "connection.hxx"
#include "protocol.hxx"

using namespace nz;

std::string hexify(const_buffer_ref buf)
{
	static constexpr std::array<char, 16> table = {{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' }};
	std::string result;
	if (buf.size)
		result.resize(buf.size * 3 - 1, ' ');
	auto source = reinterpret_cast<std::uint8_t const *>(buf.data);
	for (std::size_t k = 0; k < buf.size; k++) {
		std::uint8_t v = source[k];
		result[3 * k] = table[v >> 4];
		result[3 * k + 1] = table[v & 15];
	}
	return result;
}

class MinetestConnection {
public:
	MinetestConnection();

	void run();

private:
	void receive(const_buffer_ref buf);
	void control(ControlType ctl);
	void send_init();

	void recv_Hello();
	void recv_AuthAccept();
	void recv_AccessDenied();

	Connection con;
	buffer_reader inc;
	bytearray send_buf;
	bool ok = true;
	std::atomic<bool> connected = false;

	static std::map<ToClientCommand, void (MinetestConnection::*)()> const commands;
};

std::map<ToClientCommand, void (MinetestConnection::*)()> const MinetestConnection::commands = {
	{ToClientCommand::Hello, &MinetestConnection::recv_Hello},
};

MinetestConnection::MinetestConnection()
	: con(INADDR_LOOPBACK, 30000)
	, send_buf(4096)
{
	con.on_receive = std::bind(&MinetestConnection::receive, this, std::placeholders::_1);
	con.on_control = std::bind(&MinetestConnection::control, this, std::placeholders::_1);
}

void MinetestConnection::send_init()
{
	ts_init x;
	x.player_name = "numzero";
	while (!connected) {
		fmt::printf("Sending init\n");
		con.send(x.serialize(send_buf));
		sleep(1);
	}
}

void MinetestConnection::receive(const_buffer_ref buf)
{
	connected = true;
	fmt::printf("Content: [%s]", hexify(buf));
	inc = buffer_reader(buf);
	auto command = static_cast<ToClientCommand>(inc.read<u16>());
	auto phandler = commands.find(command);
	if (phandler == commands.end())
		return;
	auto handler = phandler->second;
	(this->*handler)();
}

void MinetestConnection::control(ControlType ctl)
{
	switch (ctl) {
		case ControlType::SetPeerId:
			std::thread(&MinetestConnection::send_init, this).detach();
			break;
		case ControlType::Disconnect:
			ok = false;
			break;
		default:
			break;
	}
}

void MinetestConnection::run()
{
	uint16_be command = 0;
	con.send(ref_object(command), false);
	while (ok)
		con.receive();
}

void MinetestConnection::recv_Hello()
{
	auto ser_version = inc.read<u8>();
	auto compression = inc.read<u16>();
	auto proto_version = inc.read<u16>();
	auto auth_support = inc.read<u32>();;
	auto player_name = inc.read_str<u16, char>();
	fmt::printf("Hello! %d:%d:%d:0x%x:%s\n", ser_version, compression, proto_version, auth_support, player_name);
}

void MinetestConnection::recv_AuthAccept()
{
	/*
		Message from server to accept auth.

		v3s16 player's position + v3f(0,BS/2,0) floatToInt'd
		u64 map seed
		f1000 recommended send interval
		u32 : supported auth methods for sudo mode
		      (where the user can change their password)
	*/
}

void MinetestConnection::recv_AccessDenied()
{
	/*
		u8 reason
		std::string custom reason (if needed, otherwise "")
		u8 (bool) reconnect
	*/
}

int main(int argc, char **argv)
{
	MinetestConnection c;
	c.run();
	return 0;
}
