#pragma once
// C
#include <cstdint>
// C++
#include <functional>
#include <map>
#include <stdexcept>
#include <unordered_map>
#include <vector>
// System
#include <netinet/in.h>
#include <sys/socket.h>
// Local
#include "buffer_reader.hxx"
#include "enum_class.hxx"
#include "linux/socket.h++"

using u8 = std::uint8_t;
using u16 = std::uint16_t;
using u32 = std::uint32_t;
using bytearray = std::vector<nz::byte>;

struct network_error: std::runtime_error {
	using runtime_error::runtime_error;
};

enum_class(PacketType,
	Control,
	Original,
	Split,
	Reliable,
);

enum_class(ControlType,
	Ack,
	SetPeerId,
	Ping,
	Disconnect,
);

struct SplitPacket
{
	std::vector<bytearray> parts;
	unsigned pending;
};

class Connection
{
public:
	Connection(in_addr_t addr, u16 port);
	~Connection();
	Connection(Connection const &) = delete;
	Connection(Connection &&) = delete;
	Connection &operator= (Connection const &) = delete;
	Connection &operator= (Connection &&) = delete;

	void receive();
	void send(nz::const_buffer_ref data, bool reliable = false);

	std::function<void(nz::const_buffer_ref packet)> on_receive;
	std::function<void(ControlType ctl)> on_control;
private:
	void read_control_ack();
	void read_control_set_peer_id();
	void read_control_ping();
	void read_control_disconnect();

	void read_content_control();
	void read_content_original();
	void read_content_split();
	void read_content_reliable();

	void read_packet_content();
	void read_network_packet(nz::const_buffer_ref packet);

	void send_ack(u16 seq);
	void send_normal(nz::const_buffer_ref data);
	void send_reliable(u16 seq, nz::const_buffer_ref data);

	void set_id(u16 id);

	nz::unistd::Socket fd;
	bytearray receive_buffer;
	nz::buffer_reader current_packet;
	std::map<u16, SplitPacket> split_packets;
	u16 my_id = 0;
	u16 rel_seq = 0;

	static nz::enum_array<ControlType, void (Connection::*)()> control_type_handlers;
	static nz::enum_array<PacketType, void (Connection::*)()> packet_type_handlers;
};
