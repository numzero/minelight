#include "connection.hxx"
// C++
#include <system_error>
// System
#include <unistd.h>
// Library
#include <fmt/printf.h>
// Local
#include "linux/socket.hxx"
#include "binary.hxx"

static constexpr u32 minetest_magic = 0x4f'45'74'03;

nz::enum_array<ControlType, void (Connection::*)()> Connection::control_type_handlers = {
	{ ControlType::Ack, &Connection::read_control_ack },
	{ ControlType::SetPeerId, &Connection::read_control_set_peer_id },
	{ ControlType::Ping, &Connection::read_control_ping },
	{ ControlType::Disconnect, &Connection::read_control_disconnect },
};

nz::enum_array<PacketType, void (Connection::*)()> Connection::packet_type_handlers = {
	{ PacketType::Control, &Connection::read_content_control },
	{ PacketType::Original, &Connection::read_content_original },
	{ PacketType::Split, &Connection::read_content_split },
	{ PacketType::Reliable, &Connection::read_content_reliable },
};

static nz::enum_array<PacketType, std::string> packet_type_names = {
	{ PacketType::Control, "Control" },
	{ PacketType::Original, "Original" },
	{ PacketType::Split, "Split" },
	{ PacketType::Reliable, "Reliable" },
};

std::string hexify(void const *data, std::size_t len);

using namespace nz;
using namespace unistd;

Connection::Connection(in_addr_t addr, u16 port)
	: fd(socket(socket_domain::inet, socket_type::dgram))
{
	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;

	address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_port = htons(0);
	bind(fd, address);

	address.sin_addr.s_addr = htonl(addr);
	address.sin_port = htons(port);
	connect(fd, address);
}

Connection::~Connection()
{
}

void Connection::set_id(u16 id)
{
	my_id = id;
}

void Connection::receive()
{
	bytearray buf(4096);
	struct sockaddr_in remote;
	auto packet = recvfrom(fd, ref_array(buf), remote);
	u32 addr = ntohl(remote.sin_addr.s_addr);
	u16 port = ntohs(remote.sin_port);
	fmt::printf("Got a packet from %d.%d.%d.%d:%d\n", (addr >> 24) & 0xff, (addr >> 16) & 0xff, (addr >> 8) & 0xff, (addr >> 0) & 0xff, port);
	read_network_packet(packet);
}

void Connection::send(const_buffer_ref data, bool reliable)
{
	if (reliable)
		send_reliable(rel_seq++, data);
	else
		send_normal(data);
}

void Connection::read_control_ack()
{
	auto seqnum = current_packet.read<u16>();
	fmt::printf("ack to %d", seqnum);
}

void Connection::read_control_set_peer_id()
{
	auto peerid = current_packet.read<u16>();
	fmt::printf("new peer id: %d", peerid);
	set_id(peerid);
}

void Connection::read_control_ping()
{
	fmt::printf("ping");
}

void Connection::read_control_disconnect()
{
	fmt::printf("disconnect");
}

void Connection::read_content_control()
{
	auto type = enum_cast<ControlType>(current_packet.read<u8>());
	(this->*control_type_handlers[type])();
	on_control(type);
}

void Connection::read_content_original()
{
	on_receive({current_packet.current, current_packet.remaining()});
}

void Connection::read_content_split()
{
	auto seqnum = current_packet.read<u16>();
	auto chunk_count = current_packet.read<u16>();
	auto chunk_num = current_packet.read<u16>();
	fmt::printf("seqnum=%d: chunk %d/%d", seqnum, chunk_num, chunk_count);
	auto p = split_packets.lower_bound(seqnum);
	if (p == split_packets.end() || p->first != seqnum) {
		p = split_packets.emplace_hint(p, seqnum, SplitPacket());
		p->second.parts.resize(chunk_count);
		p->second.pending = chunk_count;
	}
	auto &chunk = p->second.parts.at(chunk_num);
	chunk.resize(current_packet.remaining());
	current_packet.read_raw(chunk.data(), chunk.size());
	if (--p->second.pending)
		return; // more parts pending
	fmt::printf("complete; ");
	size_t total = 0;
	for (auto const &chunk: p->second.parts)
		total += chunk.size();
	fmt::printf("size=%d; ", total);
	bytearray buf(total);
	byte *wptr = buf.data();
	for (auto const &chunk: p->second.parts) {
		std::memcpy(wptr, chunk.data(), chunk.size());
		wptr += chunk.size();
	}
	split_packets.erase(p);
	current_packet = buffer_reader(ref_array(buf));
	read_content_original();
}

void Connection::read_content_reliable()
{
	auto seqnum = current_packet.read<u16>();
	fmt::printf("seqnum=%d: ", seqnum);
	send_ack(seqnum);
	read_packet_content();
}

void Connection::read_packet_content()
{
	auto type = enum_cast<PacketType>(current_packet.read<u8>());
	fmt::printf("type %s: ", packet_type_names[type]);
	(this->*packet_type_handlers[type])();
}

void Connection::read_network_packet(const_buffer_ref packet)
{
	current_packet = buffer_reader(packet);
	auto magic = current_packet.read<u32>();
	auto sender = current_packet.read<u16>();
	auto channel = current_packet.read<u8>();
	if (magic != minetest_magic)
		throw network_error("Not a Minetest packet");
	fmt::printf("Minetest packet from %d in channel %d: ", sender, channel);
	read_packet_content();
	fmt::printf("\n");
}

struct packet_network_header {
	uint32_be const magic = minetest_magic;
	uint16_be sender;
	uint8_be channel = 0;
};

struct packet_ack {
	packet_network_header header;
	uint8_be const packet_type = enum_cast(PacketType::Control);
	uint8_be const control_type = enum_cast(ControlType::Ack);
	uint16_be seqnum;
};

struct packet_original_header: packet_network_header {
	uint8_be const packet_type = enum_cast(PacketType::Original);
};

struct packet_reliable_header: packet_network_header {
	uint8_be const packet_type = enum_cast(PacketType::Reliable);
	uint16_be seqnum;
};

void Connection::send_ack(u16 seq)
{
	packet_ack ack;
	ack.header.sender = my_id;
	ack.seqnum = seq;
// 	fmt::printf("Sending ack of %d: %s\n", seq, hexify(&ack, sizeof(ack)));
	unistd::send(fd, ref_object(ack));
}

void Connection::send_normal(const_buffer_ref data)
{
	packet_original_header header;
	header.sender = my_id;
	unistd::send(fd, { ref_object(header), data });
}

void Connection::send_reliable(u16 seq, const_buffer_ref data)
{
	packet_reliable_header header;
	header.sender = my_id;
	header.seqnum = seq;
	unistd::send(fd, { ref_object(header), data });
}
