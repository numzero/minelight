// C++
#include <chrono>
#include <thread>
// System
#include <signal.h>
#include <netinet/in.h>
// Library
#include <fmt/printf.h>
// Local
#include "common/buffer.hxx"
#include "linux/socket.h++"

namespace zs = nz::unistd;
using seconds_f = std::chrono::duration<float, std::ratio<1>>;
using perf_clock = std::chrono::steady_clock;

seconds_f time() {
	static auto start = perf_clock::now();
	auto now = perf_clock::now();
	return std::chrono::duration_cast<seconds_f>(now - start);
}

float timef() {
	return time().count();
}

auto sig = SIGRTMIN + 2;

void legacy_sighandler(int signum)
{
	(void) signum;
	// no-op, the system call is interrupted already
}

void receive_thread(zs::fd_t fd)
{
	fmt::printf("%6.3f: thread: setting signal mask\n", timef());
	sigset_t mask;
	sigfillset(&mask);
	sigdelset(&mask, sig);
	pthread_sigmask(SIG_SETMASK, &mask, nullptr);

	nz::bytearray buffer(4096);
	struct sockaddr_in address;
	for (;;) {
		fmt::printf("%6.3f: thread: waiting for a packet\n", timef());
		try {
			nz::buffer_ref packet = zs::recvfrom(fd, nz::ref_array(buffer), address);
			fmt::printf("%6.3f: thread: received a packet of length %d\n", packet.size, timef());
		} catch (std::system_error const &e) {
			fmt::printf("%6.3f: thread: exception caught: std::system_error: [%d] %s\n", timef(), e.code().value(), e.what());
			if (e.code().value() != EINTR)
				break;
		}
	}
	fmt::printf("%6.3f: thread: finished\n", timef());
}

int main(int argc, char **argv)
{
	timef();

	fmt::printf("%6.3f: resetting signal mask\n", timef());
	sigset_t mask;
	sigfillset(&mask);
	sigdelset(&mask, SIGINT);
	sigdelset(&mask, SIGTERM);
	pthread_sigmask(SIG_BLOCK, &mask, nullptr);

	fmt::printf("%6.3f: setting signal handler\n", timef());
	struct sigaction sa;
	sa.sa_flags = 0;
	sigemptyset(&sa.sa_mask);
	sa.sa_handler = legacy_sighandler;
	sigaction(sig, &sa, nullptr);

	fmt::printf("%6.3f: creating socket\n", timef());
	zs::Socket fd{zs::socket(zs::socket_domain::inet, zs::socket_type::dgram)};

	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;

	fmt::printf("%6.3f: binding socket\n", timef());
	address.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
	address.sin_port = htons(28775);
	zs::bind(fd, address);

	fmt::printf("%6.3f: starting listener\n", timef());
	std::thread t(receive_thread, fd.get());
	fmt::printf("%6.3f: sleeping for 1 second\n", timef());
	sleep(1);
	fmt::printf("%6.3f: sending the signal\n", timef());
	pthread_kill(t.native_handle(), sig);
	fmt::printf("%6.3f: sleeping for 1 second\n", timef());
	sleep(1);
	fmt::printf("%6.3f: making the socket non-blocking\n", timef());
	fcntl((int) fd.get(), F_SETFL, O_NONBLOCK);
// 	zs::setsockopt<int>(fd.get(), SOL_SOCKET, SOCK_NONBLOCK, 1);
	fmt::printf("%6.3f: sleeping for 1 second\n", timef());
	sleep(1);
	fmt::printf("%6.3f: sending the signal\n", timef());
	pthread_kill(t.native_handle(), sig);
	fmt::printf("%6.3f: waiting\n", timef());
	t.join();
	fmt::printf("%6.3f: done\n", timef());
	return 0;
}
