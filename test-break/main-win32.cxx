// C
#include <cstring>
// C++
#include <chrono>
#include <system_error>
#include <thread>
// System
#include <winsock2.h>
#include <windows.h>
#include <unistd.h>
// Local
#include "common/buffer.hxx"

using seconds_f = std::chrono::duration<float, std::ratio<1>>;
using perf_clock = std::chrono::steady_clock;

seconds_f time() {
	static auto start = perf_clock::now();
	auto now = perf_clock::now();
	return std::chrono::duration_cast<seconds_f>(now - start);
}

float timef() {
	return time().count();
}

auto &&wsa_category = std::system_category;
WSAEVENT ev;

void wsacheck_bool(BOOL value, char const *fn) {
	if (!value)
		throw std::system_error(WSAGetLastError(), std::generic_category(), fn);
}

unsigned wsacheck(int value, char const *fn) {
	if (value == SOCKET_ERROR)
		throw std::system_error(WSAGetLastError(), wsa_category(), fn);
	return value;
}

void receive_thread(SOCKET fd)
{
	nz::bytearray buffer(4096);
	for (;;) {
		printf("%6.3f: thread: waiting for a packet\n", timef());
		WSABUF buf;
		buf.buf = (char *) buffer.data();
		buf.len = buffer.size();
		DWORD recvd = 0;
		DWORD flags = 0;
		DWORD flags2 = 0;
		WSAOVERLAPPED ov;
		std::memset(&ov, 0, sizeof(ov));
		ov.hEvent = ev;
		int ret = WSARecvFrom(fd, &buf, 1, nullptr, &flags, nullptr, 0, &ov, nullptr);
		printf("%6.3f: thread: got return value of %d\n", timef(), ret);
		if (ret && WSAGetLastError() != ERROR_IO_PENDING)
			printf("%6.3f: thread: socket error %d\n", timef(), WSAGetLastError());
		bool ret2 = WSAGetOverlappedResult(fd, &ov, &recvd, TRUE, &flags2);
		printf("%6.3f: thread: got overlapped result of %d; recvd %d, flags %d\n", timef(), (int) ret2, recvd, flags2);
		if (!ret2)
			break;
	}
	printf("%6.3f: thread: finished\n", timef());
}

int main(int argc, char **argv)
{
	timef();
	WSADATA wsa_info;
	int result = WSAStartup(MAKEWORD(2, 2), &wsa_info);
	if (result)
		throw std::system_error(result, wsa_category(), "WSAStartup");
	printf("%6.3f: creating socket\n", timef());
	SOCKET fd = wsacheck(WSASocket(AF_INET, SOCK_DGRAM, 0, nullptr, 0, WSA_FLAG_OVERLAPPED), "socket");
	ev = WSACreateEvent();

	struct sockaddr_in address;
	memset(&address, 0, sizeof(address));
	address.sin_family = AF_INET;

	printf("%6.3f: binding socket\n", timef());
	address.sin_addr.s_addr = htonl(INADDR_ANY);
	address.sin_port = htons(28775);
	wsacheck(bind(fd, (sockaddr *)&address, sizeof(address)), "bind");

	printf("%6.3f: starting listener\n", timef());
	std::thread t(receive_thread, fd);
	printf("%6.3f: sleeping for 1 second\n", timef());
	sleep(1);
	printf("%6.3f: sending the signal\n", timef());
	wsacheck_bool(WSASetEvent(ev), "set event");
	printf("%6.3f: waiting\n", timef());
	t.join();
	printf("%6.3f: done\n", timef());
	wsacheck_bool(WSACloseEvent(ev), "close event");
	return 0;
}
